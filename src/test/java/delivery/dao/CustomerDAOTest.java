package delivery.dao;

import delivery.application.ServletInitializer;
import delivery.configuration.DataSourceConfig;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ServletInitializer.class, DataSourceConfig.class})
@ActiveProfiles("test")
class CustomerDAOTest
{
    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;

    CustomerDAO customerDAO;

    @Test
    void getAll()
    {
        customerDAO = new CustomerDAO(new JdbcTemplate(dataSource));
        Assertions.assertEquals(customerDAO.getAll().size(), 2);
    }
}