package delivery.mapper;

import delivery.model.Order;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class OrderRowMapper implements RowMapper<Order>
{
    @Override
    public Order mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Order order = new Order();
        order.setId(rs.getLong("ID_ORDER"));
        order.setCustomer(new CustomerRowMapper().mapRow(rs, rowNum));
        order.setCreated(LocalDateTime.parse(rs.getString("CREATED_TIME"), formatter));
        order.setLastModified(LocalDateTime.parse(rs.getString("LAST_MODIFIED_TIME"), formatter));
        order.setStatus(new StatusMapper().mapRow(rs, rowNum));
        order.setOrderSum(rs.getDouble("ORDER_SUM"));

        return order;
    }
}
