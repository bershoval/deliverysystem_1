package delivery.mapper;

import delivery.model.Customer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerRowMapper implements RowMapper
{
    @Override
    public Customer mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        Customer customer = new Customer(rs.getString("PHONE"));
        return customer;
    }
}
