package delivery.model;

public enum Role
{
    ADMINISTRATOR,
    COURIER;
}
