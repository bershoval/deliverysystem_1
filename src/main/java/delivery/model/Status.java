package delivery.model;

import java.util.*;

public enum Status
{
    CREATED(1l, "Заказ создан."),
    PAYED(2l, "Заказ оплачен."),
    ASSEMBLED(3L, "Заказ собран."),
    TO_DELIVERY(4L, "Заказ передан в доставку."),
    DELIVERED(5L,"Заказ доставлен."),
    UNKNOWN(-1L, "Неверный статус заказа.");

    private final long code;
    private final String Description;

    Status(long code, String description)
    {
        this.code = code;
        Description = description;
    }

    public long getCode()
    {
        return code;
    }

    public String getDescription()
    {
        return Description;
    }

    /**
     * Возврат объекта Status по коду
     * @param code - код статуса
     * @return - объект Status
     */
    public static Status getStatusByCode(long code)
    {
        return Arrays.stream(values()).filter(x-> x.getCode() == code).findFirst().orElse(Status.UNKNOWN);
    }

    /**
     * @return - объект {@link Map<Long, Status>}, содержащий коды статусов и соответствующие описания
     */
    public static Map<Long, Status> getCodeDescription()
    {
        Map<Long, Status> codeStatus = new HashMap<>();

        for(Status s: Status.values())
        {
            codeStatus.put(s.getCode(), s);
        }
        return codeStatus;
    }

}
