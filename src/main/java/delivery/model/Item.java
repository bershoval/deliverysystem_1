package delivery.model;

import java.math.BigDecimal;

public class Item
{
    private long id;
    private String description;
    private BigDecimal price;
}
