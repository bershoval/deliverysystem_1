package delivery.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class StatusHistory
{
    private Status status;
    private Order order;
    private LocalDateTime date_set;

    public StatusHistory() {

    }

    public StatusHistory(Status status, Order order, LocalDateTime date_set) {
        this.status = status;
        this.order = order;
        this.date_set = date_set;
    }

    public Status getStatus()
    {
        return status;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public Order getOrder()
    {
        return order;
    }

    public void setOrder(Order order)
    {
        this.order = order;
    }

    public LocalDateTime getDate_set()
    {
        return date_set;
    }

    public void setDate_set(LocalDateTime date_set)
    {
        this.date_set = date_set;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
        {
            return true;
        }
        if (o == null || o.getClass() != this.getClass())
        {
            return false;
        }

        StatusHistory history = (StatusHistory) o;

        return this.order.equals(history.order)
                && this.status.equals(history.status)
                && this.date_set.equals(history.date_set);
    }

    @Override
    public String toString()
    {
        return "Статус заказа № "
                + order.getId()
                + " изменён на "
                + this.status.getDescription()
                + " "
                + this.date_set.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}
