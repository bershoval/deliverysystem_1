package delivery.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class LoggingAspect
{
    private  final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Срез точек соединения. Срез для orders.controller.* и orders.dao.*
     */
    @Pointcut("within(delivery.controller.*)" + "|| within(delivery.dao.*)")
    public void beansPointcut()
    {

    }

    /**
     * Совет применяется перед выполнением методов, указанных в срезе
     * @param joinPoint
     */
    @Before("beansPointcut()")
    public void beforeMethod(JoinPoint joinPoint)
    {
        if (log.isDebugEnabled())
        {
            log.debug("Order app enter: {}.{}() with argument[s] = {}", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
        }

        if(log.isInfoEnabled())
        {
            log.info("Order app enter: {}.{}() with argument[s] = {}", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
        }
    }

    /**
     * Совет применяется, если метод, из указанных в срезе, выбросил исключение
     * @param joinPoint
     */
    @AfterThrowing(pointcut = "beansPointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e)
    {
        log.error("Exception in {}.{}() with cause = {}", joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(), e.getCause() != null ? e.getCause() : "NULL");
    }


    /**
     * Совет применяется после выполнения методов, указанных в срезе
     * @param joinPoint
     */
    @AfterReturning("beansPointcut()")
    public void AfterMethod(JoinPoint joinPoint)
    {
        if (log.isDebugEnabled())
        {
            log.debug("Order app exit: {}.{}() with argument[s] = {}", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
        }

        if(log.isInfoEnabled())
        {
            log.info("Order app exit: {}.{}() with argument[s] = {}", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
        }

    }
}
