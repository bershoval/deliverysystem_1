package delivery.configuration;


import delivery.dao.OrderDAO;
import delivery.dao.CustomerDAO;
import delivery.dao.StatusHistoryDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
@PropertySource({"classpath:application.properties"})
@Import(DataSourceConfig.class)
public class ApplicationConfig
{

    @Bean(name = "orderDAO")
    public OrderDAO personDao(JdbcTemplate template)
    {
        return new OrderDAO(template);
    }

    @Bean(name = "statusHistoryDAO")
    public StatusHistoryDAO historyDAO(JdbcTemplate template)
    {
        return new StatusHistoryDAO(template);
    }


    @Bean(name = "customerDAO")
    public CustomerDAO customerDAO(JdbcTemplate template)
    {
        return new CustomerDAO(template);
    }
}
